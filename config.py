import os.path
# The name of the database that is to be created in the Root directory
DATABASE = "app_database.db"

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
PATH = os.path.join(BASE_DIR, DATABASE)
CONNECTIONSTRING='sqlite:///{}'.format(PATH)

# Full path to the text files
TWEETFILEPATH = os.path.join(BASE_DIR, 'resources/tweet.txt')
USERFILEPATH = os.path.join(BASE_DIR, 'resources/user.txt')

# Automatically creates database, creates tables, and populate the tables with contents of the text files
INITDATABASE = True

# Toggl if one would like to perform unit tests
TEST = True

if TEST:
    DATABASE = 'test_database.db'
    PATH = os.path.join(BASE_DIR, DATABASE)
    CONNECTIONSTRING = 'sqlite:///{}'.format(PATH)
