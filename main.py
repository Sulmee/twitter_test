from config import INITDATABASE, TEST, USERFILEPATH, TWEETFILEPATH
from app.Twitter import Twitter


if __name__ == '__main__':

    if INITDATABASE:
        import alembic.config

        alembicArgs = [
            '--raiseerr',
            'upgrade', 'head',
        ]
        alembic.config.main(argv=alembicArgs)

    if TEST:
        import app.Tests as TestCases
        import unittest
        suite = unittest.TestLoader().loadTestsFromModule(TestCases)
        unittest.TextTestRunner(verbosity=2).run(suite)

    else:
        twitter = Twitter(USERFILEPATH, TWEETFILEPATH, INITDATABASE)

        tweets = twitter.display_tweets()

        if tweets['status']:
            for t in tweets['data']:
                print(t)
        else:
            print(tweets)