from . import process_twitter, process_users
from config import USERFILEPATH, TWEETFILEPATH
import unittest


class TestMethods(unittest.TestCase):

    def test_process_users(self):
        with self.subTest():
            self.assertEqual(process_users(None), {'status': False, 'error': 'file path was not specified'})
        with self.subTest():
            self.assertEqual(process_users(''), {'status': False, 'error': 'No such file or directory'})
        with self.subTest():
            self.assertEqual(process_users(USERFILEPATH), {'status': True, 'error': ''})

    def test_process_twitter(self):
        with self.subTest():
            self.assertEqual(process_twitter(None), {'status': False, 'error': 'file path was not specified'})
        with self.subTest():
            self.assertEqual(process_twitter(''), {'status': False, 'error': 'No such file or directory'})
        with self.subTest():
            self.assertEqual(process_twitter(TWEETFILEPATH), {'status': True, 'error': ''})


if __name__ == '__main__':
    unittest.main()