import pandas as pd
from app import session_scope, UserFollower, Post, User
from sqlalchemy import and_
from config import TWEETFILEPATH, USERFILEPATH


def process_users(filepath):
    """
    Utility method to insert users into a database, it does checks to see if a user exists as well as follower

    >>> from app import process_users
    >>> process_users(None)
    {'status': False, 'error': 'file path was not specified'}
    >>> process_users('')
    {'status': False, 'error': 'No such file or directory'}
    >>> process_users(USERFILEPATH)
    {'status': True, 'error': ''}


    :param filepath: string - full path of the text file
    :return: dict - response
    """
    if filepath is not None:

        try:
            with open(filepath, 'r') as file_handle:
                lines = file_handle.read().splitlines()
                data = list(filter(lambda x: len(x) == 2 and x is not None, [i.split('follows') for i in lines]))

                df = pd.DataFrame(data, columns=['users', 'followers'])

                for row in df.itertuples():
                    # iterating through the rows of the text file and processing the users as well as followers
                    # in same iteration

                    user = getattr(row, 'users')
                    follower = getattr(row, 'followers').split(",")

                    clean_user = user.strip()
                    cleaned_follower = [i.strip() for i in follower]

                    with session_scope() as session:
                        user_exists = session.query(User).filter(User.handle == clean_user).first()
                        user_id = None

                        if user_exists is None:
                            # If the user does not exist add them
                            adding_user = User(handle=clean_user)
                            # Every user is a follower of themselves, this makes displaying posts easier
                            adding_own_follower = UserFollower(user=adding_user, follower=adding_user)

                            session.add(adding_user)
                            session.add(adding_own_follower)
                        else:
                            user_id = user_exists.user_id

                        for f in cleaned_follower:
                            # checks if the follower exists already in the Users table, we don't want duplicates
                            follower_exists = session.query(User).filter(User.handle == f).first()

                            if follower_exists is None:
                                # if the follower doesn't exist as a user, create them
                                f_adding_user = User(handle=f)
                                f_adding_follower = UserFollower(user=f_adding_user, follower=f_adding_user)

                                session.add(f_adding_user)
                                session.add(f_adding_follower)

                                # Check if the user_id is None because we don't want to query the users table again
                                # to get the user_id, if the user didn't exist previously, use the model that was
                                # created to add their follower relationship
                                #
                                # Made the assumption that if the follower did not exist as user then there is no need
                                # to check the UserFollowers table to see if there is already an entry since the two
                                # tables are related to each other
                                if user_id is None:
                                    follower_rel = UserFollower(user=adding_user, follower=f_adding_user)
                                    session.add(follower_rel)
                                else:
                                    follower_rel = UserFollower(user_id=user_id, follower=f_adding_user)
                                    session.add(follower_rel)

                            else:
                                # If the follower does exist as a user, check to see if there is already a followers
                                # relationship for the user
                                follower_id = follower_exists.user_id

                                exist_follower_rel = session.query(User)\
                                    .join(UserFollower, User.user_id == UserFollower.user_id)\
                                    .filter(and_(UserFollower.follower_id == follower_id,
                                                 User.handle == clean_user,
                                                 UserFollower.follower_id != UserFollower.user_id))\
                                    .first()

                                if exist_follower_rel is None:
                                    follower_rel = UserFollower(user_id=user_id, follower_id=follower_id)
                                    session.add(follower_rel)

            return {'status': True, 'error': ''}
        except FileNotFoundError:
            return {'status': False, 'error': 'No such file or directory'}
        except Exception as error:
            print(error)
            return {'status': False, 'error': error}

    else:
        return {'status': False, 'error': 'file path was not specified'}


def process_twitter(filepath):
    """
    Processes the twitter.txt file.
    If a post was made with out the user being specified in the user.txt file,
    this method will insert the user

    >>> from app import process_twitter
    >>> process_twitter(None)
    {'status': False, 'error': 'file path was not specified'}
    >>> process_twitter('')
    {'status': False, 'error': 'No such file or directory'}
    >>> process_twitter(TWEETFILEPATH)
    {'status': True, 'error': ''}

    :param filepath: string - full path of the text file
    :return: dict - response
    """

    if filepath is not None:

        try:
            with open(filepath, 'r') as file_handle:
                lines = file_handle.read().splitlines()
                data = list(filter(lambda x: len(x) == 2 and x is not None, [i.split('> ') for i in lines]))

                df = pd.DataFrame(data, columns=['users', 'tweet'])

                for row in df.itertuples():
                    user = getattr(row, 'users')
                    tweet = getattr(row, 'tweet')

                    clean_user = user.strip()
                    cleaned_tweet = tweet.strip()

                    with session_scope() as session:
                        user_exists = session.query(User).filter(User.handle == clean_user).first()

                        if user_exists is None:
                            # Catering for if there is a user in the twitter.txt that posted a tweet
                            # that is not included in the user.txt
                            adding_user = User(handle=clean_user)
                            # A user is a follower of themselves, this makes displaying tweets easier
                            adding_follower = UserFollower(user=adding_user, follower=adding_user)
                            adding_post = Post(user=adding_user, post=cleaned_tweet)

                            session.add(adding_user)
                            session.add(adding_follower)
                            session.add(adding_post)
                        else:
                            user_id = user_exists.user_id
                            adding_post = Post(user_id=user_id, post=cleaned_tweet)
                            session.add(adding_post)

            return {'status': True, 'error': ''}

        except FileNotFoundError:
            return {'status': False, 'error': 'No such file or directory'}
        except Exception as error:
            print(error)
            return {'status': False, 'error': error}

    else:
        return {'status': False, 'error': 'file path was not specified'}

