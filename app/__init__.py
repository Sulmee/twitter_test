from .models import Session
from contextlib import contextmanager

@contextmanager
def session_scope():
    session = Session()
    try:
        yield session
        session.commit()
    except Exception as error:
        session.rollback()
        raise
    finally:
        session.close()


from .models.users_model import User, UserFollower
from .models.posts_model import Post
from .utils.Helper import process_users, process_twitter
