from sqlalchemy import Column, DateTime, ForeignKey, Integer, LargeBinary, String, text
from sqlalchemy.orm import relationship
from . import Base


class User(Base):
    __tablename__ = 'users'

    user_id = Column(Integer, primary_key=True)
    handle = Column(String(255), nullable=False)
    created_date = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))


class UserFollower(Base):
    __tablename__ = 'user_followers'

    user_follower_id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey('users.user_id'), nullable=False, index=True)
    follower_id = Column(ForeignKey('users.user_id'), nullable=False, index=True)

    # user = relationship('User', backref='users')
    # follower = relationship('User', backref='users')

    follower = relationship('User', primaryjoin='UserFollower.follower_id == User.user_id')
    user = relationship('User', primaryjoin='UserFollower.user_id == User.user_id')
