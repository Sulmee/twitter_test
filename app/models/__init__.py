from config import CONNECTIONSTRING
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

engine = create_engine(CONNECTIONSTRING, echo=False, pool_recycle=3600)
Base = declarative_base()
Session = scoped_session(sessionmaker(bind=engine, autoflush=False))

from .users_model import *
from .posts_model import *