from sqlalchemy import Column, DateTime, ForeignKey, Integer, LargeBinary, String, text
from sqlalchemy.orm import relationship
from . import Base


class Post(Base):
    __tablename__ = 'posts'

    post_id = Column(Integer, primary_key=True)
    post = Column(String(140), nullable=False)
    user_id = Column(ForeignKey('users.user_id'), nullable=False, index=True)
    created_date = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))

    user = relationship('User')
