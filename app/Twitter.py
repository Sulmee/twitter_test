from . import UserFollower, Post, User, session_scope
from . import process_users, process_twitter
from sqlalchemy.orm import aliased


class Twitter:

    def __init__(self, user_filepath, twitter_filepath, init_db):
        self.user_filepath = user_filepath
        self.twitter_filepath = twitter_filepath
        self.init_db = init_db

        if self.init_db:
            process_users(user_filepath)
            process_twitter(twitter_filepath)

    def display_tweets(self):
        """
        Method to fetch and store tweets for it to be displayed

        :return: dict
        """
        tweets = []

        with session_scope() as session:
            try:
                get_users = session.query(User).order_by(User.handle.asc()).all()

                followers = aliased(User)

                for user in get_users:
                    query = session.query(User.handle, followers.handle.label('followers_handle'), Post.post)\
                        .join(UserFollower, User.user_id == UserFollower.user_id)\
                        .join(followers, followers.user_id == UserFollower.follower_id)\
                        .join(Post, Post.user_id == UserFollower.follower_id)\
                        .filter(User.handle == user.handle)\
                        .order_by(Post.post_id.asc())\
                        .all()

                    tweets.append(user.handle)
                    if query is not None:
                        if len(query) > 0:
                            tweets = tweets + ["\t@{0}: {1}".format(row.followers_handle, row.post) for row in query]
                return {'status': True, 'error': '', 'data': tweets}
            except Exception as error:
                return {'status': False, 'error': error, 'data': []}
