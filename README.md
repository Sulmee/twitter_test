### Twitter Assignment

**Preface:**

When looking at this problem, one could have solved this in one class/file.
What I decided to do was build a scalable solution so that it is future proofed and a possible front end/client using
React for example to display the results in a web browser.

The following is the stack/libraries that I used:

1. I used was SQLite3 as a database to insert the users, followers and tweets
2. Python3.6
3. SQLAlchemy ORM to model the various tables as a persistence layer.
4. Pandas to manipulate the data from the text files, much faster than lists/arrays
5. SessionScope to manage sqlalchemy's session, as to handle rollbacks, opening, closing connection gracefully
6. unittest to write and perform unit tests
7. alembic for version control of the models

**Requirements:**

1. Python3.6 (preferably use a virtualenv) including pip3
2. Ubuntu (Windows has not been tested)
3. SQLite3

**Assumptions:**

1. If a user in the tweets.txt is not included in the user.txt neither as a user nor follower,
they will be added as a user.
2. If a user follows a user that does not exist as a user themselves then that user will be added.
3. Every user in themselves follow themselves, this helped with getting tweets

**Installation**:

```bash
sudo apt-get update
sudo apt-get install sqlite3 libsqlite3-dev
```

```python
(Optional: activate virtualenv)
>>> pip3.6 install -r requirements.txt
```

**To Execute**

Open config.py

```python

import os.path
# The name of the database that is to be created in the Root directory
DATABASE = "app_database.db"

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
PATH = os.path.join(BASE_DIR, DATABASE)
CONNECTIONSTRING='sqlite:///{}'.format(PATH)

# Full path to the text files
TWEETFILEPATH = os.path.join(BASE_DIR, 'resources/tweet.txt')
USERFILEPATH = os.path.join(BASE_DIR, 'resources/user.txt')

# Automatically creates database, creates tables, and populate the tables with contents of the text files
INITDATABASE = True

# Toggl if one would like to perform unit tests
TEST = True

if TEST:
    DATABASE = 'test_database.db'
    PATH = os.path.join(BASE_DIR, DATABASE)
    CONNECTIONSTRING = 'sqlite:///{}'.format(PATH)

```

Toggle INITDATABASE if you would like to create the database, create tables and populate those tables
Toggle INITDATABASE and TEST if you would like to perform unit tests.

Note: INITDATABASE should only be toggled initially (each for test and getting tweets), thereafter tweets will get
duplicated, users won't get duplicated.

Change TWEETFILEPATH and USERFILEPATH if files are placed elsewhere

Lastly:

```python
>>> python3.6 main.py
```

To run the program